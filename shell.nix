let
  pkgs = import <nixpkgs> { };
in
pkgs.mkShell {
  buildInputs = [
    pkgs.wayland
    pkgs.wayland-protocols
    pkgs.xorg.libxcb
    pkgs.xorg.libX11
    pkgs.xorg.libXcursor
    pkgs.xorg.libXi
    pkgs.xorg.libXrandr
    pkgs.libxkbcommon
    pkgs.vulkan-headers
    pkgs.vulkan-tools
    pkgs.vulkan-headers
    pkgs.vulkan-loader
    pkgs.vulkan-validation-layers
    pkgs.libGL
    pkgs.pkg-config
  ];
  LD_LIBRARY_PATH = with pkgs;
    lib.makeLibraryPath [
      libxkbcommon # keyboard
      libGL
      alsaLib # sound

      vulkan-headers
      vulkan-loader

      # wayland
      wayland

      # xstuff
      xorg.libX11
      xorg.libXcursor
      xorg.libXi
      xorg.libXrandr
    ];
}