use floem::{
    menu::{Menu, MenuItem},
    style::Style,
    view::View,
    views::{label, v_stack, Decorators},
};

pub fn app_view(theme: Style) -> impl View {
    v_stack((
        label(move || "Hello"),
        label(move || "Click Me").context_menu(|| {
            Menu::new("Title")
                .entry(MenuItem::new("First"))
                .entry(MenuItem::new("Second"))
                .separator()
                .entry(MenuItem::new("Another"))
                .entry(MenuItem::new("Too"))
                .entry(MenuItem::new("Much"))
        }),
    ))
    .style(move |_| theme.clone().width_full())
}
