use floem::{
    event::{Event, EventListener},
    new_window,
    peniko::Color,
    reactive::{create_rw_signal, create_signal},
    style::{CursorStyle, Position, Style},
    view::View,
    views::{container, h_stack, label, v_stack, Decorators},
    widgets::button,
    window::WindowConfig,
    EventPropagation,
};

pub mod app;

const SIDEBAR_WIDTH: f64 = 100.0;

fn draggable_view() -> impl View {
    let sidebar_width = create_rw_signal(SIDEBAR_WIDTH);
    let is_sidebar_dragging = create_rw_signal(false);
    let theme = Style::new()
        .background(Color::rgb8(40, 44, 52))
        .color(Color::LIGHT_GRAY);

    let side_bar = v_stack((label(|| "Sidebar"),))
        .style(move |s| s.background(Color::LIGHT_BLUE).width(sidebar_width.get()));
    let dragger = label(|| "")
        .style(move |s| {
            s.width(10)
                .position(Position::Absolute)
                .z_index(10)
                .inset_top(0)
                .inset_bottom(0)
                .border_left(1)
                .border_color(Color::YELLOW)
                .inset_left(sidebar_width.get())
                .background(Color::RED)
        })
        .draggable()
        .on_event(EventListener::DragStart, move |_| {
            is_sidebar_dragging.set(true);
            EventPropagation::Continue
        })
        .on_event(EventListener::DragEnd, move |_| {
            is_sidebar_dragging.set(false);
            EventPropagation::Continue
        });
    let main_window = v_stack((container(v_stack((
        label(|| "Main"),
        button(move || format!("Open")).on_click_stop(move |_| {
            let new_theme = theme.clone();
            new_window(
                move |_| app::app_view(new_theme),
                Some(WindowConfig::default()),
            );
        }),
    )))
    .style(|s| s.flex_col().items_start()),))
    .style(|s| {
        s.background(Color::LIGHT_GRAY)
            .margin_left(10)
            .flex_col()
            .min_width(0)
            .flex_grow(1.0)
    });

    h_stack((side_bar, dragger, main_window))
        .on_event(EventListener::PointerMove, move |event| {
            let pos = match event {
                Event::PointerMove(p) => p.pos,
                _ => (0.0, 0.0).into(),
            };

            if is_sidebar_dragging.get() {
                sidebar_width.set(pos.x);
            }
            EventPropagation::Continue
        })
        .style(|s| s.width_full().height_full())
}

fn new_app_view() -> impl View {
    let theme = Style::new()
        .background(Color::rgb8(40, 44, 52))
        .color(Color::LIGHT_GRAY);
    let main_theme = theme.clone().width_full();
    let side_theme = theme.clone();
    let new_window_theme = theme.clone();
    let (show_sidebar, set_show_sidebar) = create_signal(true);

    let sidebar_width = create_rw_signal(SIDEBAR_WIDTH);
    let is_sidebar_dragging = create_rw_signal(false);

    let slider = label(|| "")
        .style(move |s| {
            s.position(Position::Absolute)
                .z_index(10)
                .inset_top(0)
                .inset_bottom(0)
                .inset_left(sidebar_width.get() - 10.0)
                .width(10)
                .border_left(1)
                .border_color(Color::rgb8(205, 205, 205))
                .hover(|s| {
                    s.border_left(2)
                        .border_color(Color::rgb8(41, 98, 218))
                        .cursor(CursorStyle::ColResize)
                })
                .apply_if(is_sidebar_dragging.get(), |s| {
                    s.border_left(2).border_color(Color::rgb8(41, 98, 218))
                })
        })
        .draggable()
        .on_event(EventListener::DragStart, move |_| {
            is_sidebar_dragging.set(true);
            EventPropagation::Continue
        })
        .on_event(EventListener::DragEnd, move |_| {
            is_sidebar_dragging.set(false);
            EventPropagation::Continue
        });
    let id = slider.id();

    h_stack((
        v_stack((
            label(move || format!("Sidebar")),
            label(move || format!("Width {}", sidebar_width.get())),
            label(move || format!("Is Dragging {}", is_sidebar_dragging.get())),
        ))
        .style(move |_| {
            let s = side_theme.clone().width(sidebar_width.get() - 1.0);
            if !show_sidebar.get() {
                s.hide()
            } else {
                s
            }
        }),
        slider,
        v_stack((
            label(move || format!("Main Window")),
            button(move || format!("Open")).on_click_stop(move |_| {
                let new_theme = new_window_theme.clone();
                new_window(
                    move |_| app::app_view(new_theme),
                    Some(WindowConfig::default()),
                );
            }),
            button(|| "Toggle").on_click_stop(move |_| set_show_sidebar.update(|v| *v = !*v)),
            button(|| "Inspect").on_click_stop(move |_| id.inspect()),
        ))
        .style(move |_| {
            let s = main_theme.clone();
            s.width_full()
        }),
    ))
    .style(|s| s.width_full())
    .on_event(EventListener::PointerMove, move |event| {
        let pos = match event {
            Event::PointerMove(p) => p.pos,
            _ => (0.0, 0.0).into(),
        };
        if is_sidebar_dragging.get() {
            sidebar_width.set(pos.x);
        }
        EventPropagation::Continue
    })
}

fn main() {
    //floem::launch(new_app_view)
    floem::launch(draggable_view)
}
